<?php
namespace Home\Controller;
use Think\Controller\RestController;
class UserController extends RestController {
    protected $allowMethod    = array('get','post','put'); // REST允许的请求类型列表
    protected $allowType      = array('html','xml','json'); // REST允许请求的资源类型列表
    protected $defaultType      = "json";

    /**
     * 通过post请求传递过来的
     * uid
     * username
     * password
     * name
     * email
     * telephone
     * birthday
     * sex
     * state
     * code
     */
    Public function register(){
        //I('post.name','','htmlspecialchars');
        $User = D('User');
        $uid = md5(uniqid());
        $User->uid = $uid; //使用MD5生成唯一id
        $User->username = I('post.username');
        $User->password = I('post.password');
        $User->name = I('post.name');
        $User->email = I('post.email');
        $User->telephone = I('post.telephone');
        $User->sex = I('post.sex');
        $User->add();
        $user = $User->find( $uid);

        $res = json_encode($user, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     * * 通过post请求传递过来的
     * username
     * password
     *
     */
    Public function login(){
        $User = D('User');
        $user = $User->where('username="'.I('post.username').'"  AND password="'.I('post.password').'"')->find();
        $res = json_encode($user, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    public function test(){
        $User = D('User');
        $user = $User->where('username="'."mmm".'"  AND password="'."123456".'"')->find();
        $res = json_encode($user, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }


}