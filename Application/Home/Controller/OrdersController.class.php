<?php
namespace Home\Controller;
use function Sodium\add;
use Think\Controller\RestController;
class OrdersController extends RestController {
    protected $allowMethod    = array('get','post','put'); // REST允许的请求类型列表
    protected $allowType      = array('html','xml','json'); // REST允许请求的资源类型列表
    protected $defaultType      = "json";


    /**
     * 请求：post
     * 参数
     * uid
     *
     * 返回:该uid的所有订单
     */
    Public function getOrdersByUid(){
        $Orders = D("Orders");
        $orders = $Orders->where('uid="'.I("post.uid").'"')->select();
        /*$Orderitem = D("OrderItem");
        for($i=1; $i<$orders.length; $i++){
            $orders['oid'];
            $re = $Orderitem->where('oid="'.$orders['oid'].'"')->select();
            $orders->list->add();
        }*/
        $res = json_encode($orders, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     * 请求：post
     * 参数
     * oid
     *
     * 返回:该oid的所有订单项
     */
    Public function getOrderitemsByOid(){
        $Orderitem = D("Orderitem");
        $orderitem = $Orderitem->where('oid="'.I("post.oid").'"')->select();
        $res = json_encode($orderitem, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     * 请求：post
     * 参数 json串
     * 例：
     * {"uid":"1","total":"1234","orderitem":[{"count":"1","subtotal":"2298","pid":"11","oid":"11"},{"count":"1","subtotal":"2299","pid":"31","oid":"11"}]}
     *
     * 复制上面字符串到在线json查询就可以看到清晰的结构
     */
    Public function addOrders(){
        $json = $GLOBALS['HTTP_RAW_POST_DATA'];
        $order = json_decode($json);
        $data =  $order->orderitems;
        $uid =  $order->uid;
        $total = $order->total;
        $Orderitem = D("Orderitem");
        for($i=0; $i<count($data); $i++){
            $Orderitem->itemid = md5(uniqid());
            $Orderitem->count = $data[$i]->count;
            $Orderitem->subtotal = $data[$i]->subtotal;
            $Orderitem->pid = $data[$i]->pid;
            $Orderitem->oid = $data[$i]->oid;
            $Orderitem->add();
        }
        $Orders = D("Orders");
        $Orders->oid = md5(uniqid());
        $Orders->ordertime = date("Y-m-d");
        $Orders->total = $total;
        $Orders->state = 0;
        $Orders->uid = $uid;
        $result = $Orders->add();
        $res = json_encode($result, JSON_UNESCAPED_UNICODE);
        $this->response($res);


    }


    Public function test(){
        $res=json_decode("{\"uid\":\"1\",\"total\":\"1234\",\"orderitem\":[{\"count\":\"1\",\"subtotal\":\"2298\",\"pid\":\"11\",\"oid\":\"11\"},{\"count\":\"1\",\"subtotal\":\"2299\",\"pid\":\"31\",\"oid\":\"11\"}]}");
        dump( $res->orderitem[0]->pid);
    }

    Public function test2(){
        $Model = M();
        $orders = $Model->field('item.*,o.total,o.state')
            ->table(array('ff_orderitem'=>'item','ff_orders'=>'o'))
            ->where('o.uid = "20f6d30426e7bf55e1881be5a83b1070"')->select();
        $res = json_encode($orders, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }


}