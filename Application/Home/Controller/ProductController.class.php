<?php
namespace Home\Controller;
use Think\Controller\RestController;
class ProductController extends RestController {
    protected $allowMethod    = array('get','post','put'); // REST允许的请求类型列表
    protected $allowType      = array('html','xml','json'); // REST允许请求的资源类型列表
    protected $defaultType      = "json";

    /**
     *最热商品，最多返回20条（没有进行分页查询）
     */
    Public function getHotProducts(){
        //I('post.name','','htmlspecialchars');
        $Product = D('Product');
        $product = $Product->where('is_hot=1')->order('pdate')->limit(20)->select();
        $res = json_encode($product, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     *最新商品，最多返回20条（没有进行分页查询）
     */
    Public function getNewProducts(){
        $Product = D('Product');
        $product = $Product->order('pdate')->limit(20)->select();
        $res = json_encode($product, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     *分类
     */
    Public function getCategorys(){
        $Category = D('Category');
        $categorys = $Category->select();
        //dump($categorys);
        $res = json_encode($categorys, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     * 添加分类
     *
     * cname
     */
    Public function addCategory(){
        $Category = D('Category');
        $Category->cid = md5(uniqid());
        $Category->cname = I('post.cname');
        $category = $Category->add();
        $res = json_encode($category, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }

    /**
     * get请求
     *根据分类id，最多返回20条（没有进行分页查询）
     *
     * cid
     */
    Public function getProductsByCid(){
        $Product = D('Product');
        $product = $Product->where('cid="'.I('get.cid').'"')->limit(20)->select();
        $res = json_encode($product, JSON_UNESCAPED_UNICODE);
        $this->response($res);
    }


}